/*
 * HAL_GPS.c
 *
 *  Created on: 27 May 2020
 *      Author: Hakan TAMBUĞA && Tunahan ÖZKEZER
 */

#include "HAL_GPS.h"

UART_HandleTypeDef *gpsUart;

void HAL_GPS_Init(UART_HandleTypeDef *gps_Uart)
{
	gpsUart = gps_Uart;
	GPS.rxIndex=0;
	HAL_UART_Receive_IT(gpsUart,&GPS.rxTmp,1);
}

double HAL_GPS_convertDegMinToDecDeg (float degMin)
{
  double min = 0.0;
  double decDeg = 0.0;

  //get the minutes, fmod() requires double
  min = fmod((double)degMin, 100.0);

  //rebuild coordinates in decimal degrees
  degMin = (int) ( degMin / 100 );
  decDeg = degMin + ( min / 60 );

  return decDeg;
}

void HAL_GPS_CallBack(void)
{
	GPS.LastTime=HAL_GetTick();
		if(GPS.rxIndex < sizeof(GPS.rxBuffer)-2)
		{
			GPS.rxBuffer[GPS.rxIndex] = GPS.rxTmp;
			GPS.rxIndex++;
		}
		HAL_UART_Receive_IT(gpsUart,&GPS.rxTmp,1);
}

void HAL_GPS_GetData(void)
{
	  if( (HAL_GetTick()-GPS.LastTime>50) && (GPS.rxIndex>0))
	  	{

	  		char	*str;
	  		str=strstr((char*)GPS.rxBuffer,"$GPGGA,");

	  		if(str!=NULL)
	  		{

	  			memset(&GPS.GPGGA,0,sizeof(GPS.GPGGA));
	  			sscanf(str,"$GPGGA,%2hd%2hd%2hd.%3hd,%f,%c,%f,%c,%hd,%hd,%f,%f,%c,%hd,%s,*%2s\r\n",&GPS.GPGGA.UTC_Hour,&GPS.GPGGA.UTC_Min,&GPS.GPGGA.UTC_Sec,&GPS.GPGGA.UTC_MicroSec,&GPS.GPGGA.Latitude,&GPS.GPGGA.NS_Indicator,&GPS.GPGGA.Longitude,&GPS.GPGGA.EW_Indicator,&GPS.GPGGA.PositionFixIndicator,&GPS.GPGGA.SatellitesUsed,&GPS.GPGGA.HDOP,&GPS.GPGGA.MSL_Altitude,&GPS.GPGGA.MSL_Units,&GPS.GPGGA.AgeofDiffCorr,GPS.GPGGA.DiffRefStationID,GPS.GPGGA.CheckSum);
	  			if(GPS.GPGGA.NS_Indicator==0)
	  				GPS.GPGGA.NS_Indicator='-';
	  			if(GPS.GPGGA.EW_Indicator==0)
	  				GPS.GPGGA.EW_Indicator='-';
	  			if(GPS.GPGGA.Geoid_Units==0)
	  				GPS.GPGGA.Geoid_Units='-';
	  			if(GPS.GPGGA.MSL_Units==0)
	  				GPS.GPGGA.MSL_Units='-';
	  			GPS.GPGGA.LatitudeDecimal=HAL_GPS_convertDegMinToDecDeg(GPS.GPGGA.Latitude);
	  			GPS.GPGGA.LongitudeDecimal=HAL_GPS_convertDegMinToDecDeg(GPS.GPGGA.Longitude);

	  		}
	  		memset(GPS.rxBuffer,0,sizeof(GPS.rxBuffer));
	  		GPS.rxIndex=0;
	  	}
	  	HAL_UART_Receive_IT(gpsUart,&GPS.rxTmp,1);

}

float HAL_GPS_GetLongtitude(void)
{
	return GPS.GPGGA.LongitudeDecimal;
}

float HAL_GPS_GetLatitude(void)
{
	return GPS.GPGGA.LatitudeDecimal;
}

float HAL_GPS_GetAltitude(void)
{
	return GPS.GPGGA.MSL_Altitude;
}

uint8_t HAL_GPS_ConnectedSatellites(void)
{
	return GPS.GPGGA.SatellitesUsed;
}

