/*
 * HAL_GPS.h
 *
 *  Created on: 27 May 2020
 *      Author: Hakan TAMBUĞA && Tunahan ÖZKEZER
 */

#ifndef INC_HAL_GPS_H_
#define INC_HAL_GPS_H_

#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "string.h"


typedef struct
{
	uint8_t			UTC_Hour;
	uint8_t			UTC_Min;
	uint8_t			UTC_Sec;
	uint16_t		UTC_MicroSec;

	float				Latitude;
	double			LatitudeDecimal;
	char				NS_Indicator;
	float				Longitude;
	double			LongitudeDecimal;
	char				EW_Indicator;

	uint8_t			PositionFixIndicator;
	uint8_t			SatellitesUsed;
	float				HDOP;
	float				MSL_Altitude;
	char				MSL_Units;
	float				Geoid_Separation;
	char				Geoid_Units;

	uint16_t		AgeofDiffCorr;
	char				DiffRefStationID[4];
	char				CheckSum[2];

}GPGGA_t;


typedef struct
{
	uint8_t		rxBuffer[512];
	uint16_t	rxIndex;
	uint8_t		rxTmp;
	uint32_t	LastTime;

	GPGGA_t		GPGGA;

}GPS_t;

GPS_t GPS;



void HAL_GPS_Init(UART_HandleTypeDef *gps_Uart);
double HAL_GPS_convertDegMinToDecDeg (float degMin);
void HAL_GPS_CallBack(void);
//void HAL_GPS_GetData(void);
float HAL_GPS_GetLongtitude(void);
float HAL_GPS_GetLatitude(void);
float HAL_GPS_GetAltitude(void);
uint8_t HAL_GPS_ConnectedSatellites(void);

#endif /* INC_HAL_GPS_H_ */
